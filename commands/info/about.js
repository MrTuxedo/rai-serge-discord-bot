const { EmbedBuilder } = require('discord.js');

module.exports = {
  name: "about",
  description: "Info about the bot",
  run: async (client, interaction) => {
    const specsFields = [
      {
        name: "Processor",
        value: "AMD Ryzen 7 1700X (16) @ 3.950GHz"
      },
      {
        name: "Memory",
        value: "32 GB RAM"
      },
      {
        name: "Chat Threads",
        value: "8"
      },
      {
        name: "Memory Speed",
        value: "2933 MT/s"
      },
      {
        name: "Video Card",
        value: "AMD ATI Radeon R9 290 (no ROCm support)"
      },
      {
        name: "Other",
        value: "Xubuntu 22.04\nSingle Task Only - 256 Max Token Output"
      }
    ];
    
    const embed = new EmbedBuilder()
    .setColor("#FF0000")
      .setTitle("About rAI")
      .setDescription(`Latency : ${client.ws.ping}ms\n\nrAI is a bot managed by \`MrTuxedo#0765\` \nRunning GPT4ALL and LLama 7B/7B-native.`)
      .addFields(specsFields)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    
    interaction.followUp({ embeds: [embed] });
  },
};
