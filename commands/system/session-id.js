const { EmbedBuilder } = require('discord.js');
var unirest = require('unirest');
const jsonfile = require('jsonfile')

module.exports = {
  name: "view-session-id",
  description: "View your currently assigned session ID",
  private: true,
  run: async (client, interaction) => {
    const file = './cache/' + interaction.user.id

    jsonfile.readFile(file, function (err, session) {
      if (err) return interaction.editReply('Please create a session using /create-session.');

      const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle("Your current session ID")
      .setDescription(`**Session ID:** \`${session.id}\`\n\nThe above ID is a unique session memories are stored for processing.`)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    
    interaction.followUp({ embeds: [embed] });
    })

  },
};