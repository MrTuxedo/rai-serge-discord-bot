const axios = require('axios');
const jsonfile = require('jsonfile');
let sessionID;

function parseQuestionsAndAnswers(jsonStr) {
    const obj = JSON.parse(jsonStr);
    const questions = obj.questions;
    const answers = questions.map(q => q.answer);
    return { questions, answers };
}

module.exports = {
    name: "view-session-history",
    description: "View your current conversation.",
    run: async (client, interaction) => {
        const file = './cache/' + interaction.user.id;

        await jsonfile.readFile(file, async function (err, session) {

            if (err) return interaction.editReply('Please create a session using /create-session.');

            sessionID = session.id;

            if (!sessionID) {
                console.log("No Session!");
                isProcessing = false;
                return;
            }
            const headers = {
                'authority': process.env.PUBLIC_URL,
                'accept': 'text/event-stream',
            };

            const response = await axios.get(`http://${process.env.INTERNAL_IP}:${process.env.SERGE_PORT}/api/chat/${sessionID}`, {
                headers
            });

            if (!response.data.questions) return interaction.editReply("You have no history in this session yet :) ");

            const result = parseQuestionsAndAnswers(JSON.stringify(response.data));
            let pairsString = "";
            for (const question of result.questions) {
                pairsString += `***Question:*** ${question.question}\n***Answer:*** ${question.answer}\n`;
            }
            interaction.editReply(pairsString);
        })
    },
};