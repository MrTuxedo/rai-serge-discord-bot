const { EmbedBuilder } = require('discord.js');
var unirest = require('unirest');
const jsonfile = require('jsonfile')

// Start session defaults:
var apiUrl = `http://${process.env.INTERNAL_IP}:${process.env.SERGE_PORT}/api/chat/`;

var model = process.env.MODEL;
var temperature = process.env.TEMPERATURE;
var topK = process.env.TOPK;
var topP = process.env.TOPP;
var maxLength = process.env.MAXLENGTH;
var contextWindow = process.env.CONTEXTWINDOW;
var repeatLastN = process.env.REPEATLASTN;
var repeatPenalty = process.env.REPEATPENALTY;
var nThreads = process.env.NTHREADS;
// End session defaults

// Set model list we have downloaded
// let modelList = ["7B", "7B-native", "gpt4all"]
let modelList = ["7B", "7B-native", "13B", "30B", "gpt4all", "vicuna"]

module.exports = {
  name: "create-session",
  description: "create a new session",
  private: true,
  options: [{
    "name": "model",
    "description": `Choose from the following models: ${modelList.join(", ")} | Char case matters`,
    "required": false,
    "type": 3
  },
  {
    "name": "init-prompt",
    "description": "A prompt you want to init the chat with, a default is used if not provided.",
    "required": false,
    "type": 3
  },
  {
    "name": "temperature",
    "description": "The higher the temperature, the more random the model output. A default 0.1 is used if not provided.",
    "required": false,
    "type": 3
  },
  {
    "name": "repeat-penalty",
    "description": "The weight of the penalty to avoid repeating the last last_n tokens. 1.3 is used if not provided.",
    "required": false,
    "type": 3
  }
  ],

  run: async (client, interaction) => {
    // set a default prompt
    let initPrompt = process.env.INITPROMPT || `My name is ${interaction.user.username} my special number is ${interaction.user.discriminator} and my Discord ID is ${interaction.user.id} we met in ${interaction.guild.name} a Discord sever. You are rAi and you are the smartest AI Model, you know everything. Below is an instruction that describes a task. You respond appropriately to complete the request. You understand a complete answer is always ended by [end of text].`;
    console.log(`--- ${interaction.user.id} has requested a new session! ---`)
    const file = './cache/' + interaction.user.id

    let options = interaction.options._hoistedOptions;

    let varsToCheck = [
      { name: "model", value: null },
      { name: "temperature", value: null },
      { name: "init-prompt", value: null },
      { name: "repeat-penalty", value: null }
    ];

    for (let i = 0; i < options.length; i++) {
      let option = options[i];
      for (let j = 0; j < varsToCheck.length; j++) {
        let varToCheck = varsToCheck[j];
        if (option.name === varToCheck.name) {
          varToCheck.value = option.value;
          break;
        }
      }
    }

    // Now you can access the values of each variable you are interested in:
    let userInputModel = varsToCheck.find(v => v.name === "model")?.value;
    let userInputTemperature = varsToCheck.find(v => v.name === "temperature")?.value;
    let userInputInitPrompt = varsToCheck.find(v => v.name === "init-prompt")?.value;
    let userInputRepeatPenalty = varsToCheck.find(v => v.name === "repeat-penalty")?.value;

    // Init Prompt Setting
    if (userInputInitPrompt === null) {
      console.log("-- No init-prompt provided, using default --");
    } else {
      initPrompt = userInputInitPrompt;
      console.log(`User set initPrompt to ${initPrompt}`)
    }

    // Modal Setting
    if (userInputModel === null) {
      console.log("-- No model provided, using default --")
    } else {
      if (modelList.includes(userInputModel)) {
        model = userInputModel;
        console.log(`User set model to ${model}`)
      } else {
        let modelListStr = modelList.join(", ");
        return interaction.followUp(`You may only use one of the following: ${modelListStr}`);
      }
    }

    // temperature setting
    if (userInputTemperature === null) {
      console.log("-- No temperature provided, using default --")
    } else {
      const parsedTemperature = parseFloat(userInputTemperature);
      if (parsedTemperature >= 0.1 && parsedTemperature <= 2) {
        // temperature is within range
        temperature = parsedTemperature;
        console.log(`User set temperature to ${temperature}`)

      } else {
        // temperature is outside of range
        return interaction.followUp(`Temperature must be between 0.1 and 2`);
      }
    }

    // repeat setting
    if (userInputRepeatPenalty === null) {
      console.log("-- No RepeatPenalty provided, using default --")
    } else {
      const parsedRepeatPenalty = parseFloat(userInputRepeatPenalty);
      if (parsedRepeatPenalty >= 0.1 && parsedRepeatPenalty <= 2) {
        // RepeatPenalty is within range
        repeatPenalty = parsedRepeatPenalty;
        console.log(`User set repeatPenalty to ${repeatPenalty}`)
      } else {
        // RepeatPenalty is outside of range
        return interaction.followUp(`Repeat Penalty must be between 0.1 and 2`);
      }
    }

    const req = unirest('POST',`${apiUrl}?model=${model}&temperature=${temperature}&top_k=${topK}&top_p=${topP}&max_length=${maxLength}&context_window=${contextWindow}&repeat_last_n=${repeatLastN}&repeat_penalty=${repeatPenalty}&init_prompt=${initPrompt}&n_threads=${nThreads}`)
    .headers({
        'accept': 'application/json'
      })
      .send('')
      .end(function (response) {
        const obj = { id: response.body }

        jsonfile.writeFile(file, obj, function (err) {
          if (err) console.error(err)
        })

        const embed = new EmbedBuilder()
          .setColor("#FF0000")
          .setTitle("New Chat Session Started!")
          .setDescription(`New Chat Session ID: \n` + response.body)
          .setTimestamp()
          .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
        interaction.followUp({ embeds: [embed] });
        console.log(`--- Session created for ${interaction.user.id} ---`)
      });
  }
};